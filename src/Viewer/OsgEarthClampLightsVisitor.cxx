//
// Written by Jeff Biggs, Simperative Technologies LLC
//
// Copyright (C) 2014 Jeff Biggs, jeff.biggs@simperative.com
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//
// $Id$

#ifdef ENABLE_OSGEARTH

#include <osg/TextureRectangle>
#include <osg/Texture2D>
#include <osg/ComputeBoundsVisitor>
#include <osgDB/WriteFile>
#include <osgEarth/VirtualProgram>
#include <osgEarth/ElevationQuery>
#include <Airports/airport.hxx>
#include <Airports/runwaybase.hxx>
#include <Airports/runways.hxx>
#include <osgViewer/Viewer>
#include "renderer.hxx"
#include <osgText/Text>
#include <Main/globals.hxx>
#include <Main/fg_props.hxx>
#include <boost/format.hpp>
#include <osgDB/ReadFile>
#include <osg/MatrixTransform>
#include <GL/glu.h>
#include <osgDB/FileNameUtils>
#include <Scenery/scenery.hxx>
#include <boost/algorithm/string.hpp>    
#include <simgear/scene/util/OsgMath.hxx>
#include "OsgEarthClampLightsVisitor.hxx"
#include <simgear/scene/util/OsgEarthHeightCache.hxx>

OsgEarthClampLightsVisitor::OsgEarthClampLightsVisitor(
                    std::string airportName,
                    osg::MatrixTransform* transformNode,
                    SGOffsetTransform* sgXform)
:   osg::NodeVisitor(osg::NodeVisitor::TRAVERSE_ALL_CHILDREN),
    m_AirportName(airportName),
    m_ClampPhase(NotClampedPhase),
    m_IsClampedXform(false),
    m_TransformNode(transformNode),
    m_TransformMat(osg::Matrixd::identity()),
    m_SGTransformNode(sgXform),
    m_GeodPos(SGGeod::fromCart(toSG(transformNode->getMatrix().getTrans()))),
    m_DrawableList(),
    m_DrawableIndex(0),
    m_VertexIndex(0),
    m_VertexCount(0),
    m_ProcessedVertexCount(0)
{
}

void OsgEarthClampLightsVisitor::countLights()
{
    m_VertexCount = 0;
    m_ProcessedVertexCount = 0;

    for (unsigned int drawI = 0; drawI < m_DrawableList.size(); ++drawI) {
        if (m_DrawableList[drawI].valid()) {
            osg::Drawable* drawable = m_DrawableList[drawI].get();
            if (drawable->asGeometry() != NULL) {
                osg::Array *va = drawable->asGeometry()->getVertexArray();
                osg::Vec3Array *a = static_cast<osg::Vec3Array*>(va);
                if (a != NULL) {
                    for (unsigned int vertI = 0; vertI < a->size(); ++vertI) {
                        m_VertexCount++;
                    }
                }
            }
        }
    }
}

void OsgEarthClampLightsVisitor::Process(osg::Node* subGraph)
{
    m_ClampPhase = NotClampedPhase;

    if ((m_IsClampedXform == false) && 
        simgear::OsgEarthHeightCache::Instance()->IsWithinTolerance()) 
    {
        // transformation has not been clamped yet 

        double heightMsl = 0.0;
        osg::Vec3d normal;
        bool isHit = simgear::OsgEarthHeightCache::Instance()->
            FindTerrainHeightInScene(m_GeodPos, heightMsl, normal);
        if (isHit) {
            m_GeodPos.setElevationM(heightMsl);
            m_TransformMat = m_TransformNode->getMatrix();
            m_TransformMat.setTrans(toOsg(SGVec3d::fromGeod(m_GeodPos)));
            m_IsClampedXform = true;
        }
    }

    if (m_IsClampedXform && m_TransformNode.valid()) {
        // update transformation node's matrix
        m_TransformNode->setMatrix(m_TransformMat);
    }


    if ((m_DrawableList.size() == 0) && (subGraph != NULL)) {
        // populate the drawable list from the subgraph
        subGraph->accept(*(osg::NodeVisitor*)this);
        // count the number of lights that are to be clamped
        countLights();

        SG_LOG(SG_TERRAIN, SG_INFO, 
            "[OsgEarthClampLightsVisitor] Initial Processing [" << m_AirportName 
            << "] "<< "[" 
            << m_VertexCount << " lights]");

    }
        
    const osg::Timer& timer = *osg::Timer::instance();
    osg::Timer_t startT = timer.tick();

    for (unsigned int drawI = 0; drawI < m_DrawableList.size(); ++drawI) {

        if (m_DrawableIndex != 0) {
            // overwrite index for drawable (reentrant)
            drawI = m_DrawableIndex;
            m_DrawableIndex = 0;
        }

        osg::Drawable* drawable = m_DrawableList[drawI].get();
        if ((drawable != NULL) && (drawable->asGeometry() != NULL)) {
            osg::Array *va = drawable->asGeometry()->getVertexArray();
            osg::Vec3Array *a = static_cast<osg::Vec3Array*>(va);
            if (a != NULL) {
                for (unsigned int vertI = 0; vertI < a->size(); ++vertI) {

                    if (m_VertexIndex != 0) {
                        // overwrite index for vertex (reentrant)
                        vertI = m_VertexIndex; 
                        m_VertexIndex = 0;
                    }

                    // update vertex's position in vertex array
                    UpdateVertex((*a)[vertI]);

                    // increment processed vertex count
                    m_ProcessedVertexCount++;

                    // monitor clamping dt
                    double dt = timer.delta_s(startT, timer.tick());
                    
                    // in progress
                    m_ClampPhase = InProgressPhase;

                    // xxx make this a property 
                    const float maxProcesstime = 0.016;
                    if (dt > maxProcesstime) {

                        // --- allocated time slice exceeded ---

                        // save current drawable index
                        m_DrawableIndex = drawI;

                        // increment vertex index to next 
                        m_VertexIndex = vertI + 1;

                        if (m_VertexIndex >= a->size()) {
                            // --- was last vertex for this drawable ---

                            // clear vertex index and next drawable
                            m_VertexIndex = 0;
                            // increment to next drawable
                            m_DrawableIndex = drawI + 1;
                            if (m_DrawableIndex >= m_DrawableList.size()) {
                                // was last drawable, so complete
                                m_DrawableIndex = 0;
                                m_ClampPhase =  IsClampedPhase;
                            }
                        }

                        if (m_ClampPhase == InProgressPhase) {
                            SG_LOG(SG_TERRAIN, SG_INFO, 
                                "[OsgEarthClampLightsVisitor] time slice exceeded [" 
                                << m_AirportName 
                                << "] "<< "[" 
                                << std::setprecision(4) 
                                << GetProgressPercentage() << "%]");
                            // set phase to reentrant for return
                            m_ClampPhase = ReentrantPhase;
                            break;
                        }
                    }
                }
                // mark drawable as dirty
                drawable->asGeometry()->dirtyDisplayList();
                drawable->asGeometry()->dirtyBound();
            }

            if (m_ClampPhase == ReentrantPhase){
                // pop out of drawable loop
                break;
            } else {
                // complete
                m_ClampPhase = IsClampedPhase;
            }
        }
    }

    if (m_ClampPhase == IsClampedPhase) {
        SG_LOG(SG_TERRAIN, SG_INFO, 
            "[OsgEarthClampLightsVisitor] completed light clamping ["
            << m_AirportName << "]" << "[" 
            << std::setprecision(4) << GetProgressPercentage() << "%]");
    }
}

void OsgEarthClampLightsVisitor::apply(osg::Geode &node) 
{
    for (unsigned int i = 0; i < node.getNumDrawables(); ++i) {
        osg::Drawable *drawable = node.getDrawable(i);

        m_DrawableList.push_back(drawable);
    }
}


void OsgEarthClampLightsVisitor::UpdateVertex(osg::Vec3 &v) 
{
    osg::Vec3d cart = v * m_TransformNode->getMatrix();
    // set vertex height (altitude meters) relative to transform node's altitude
    double heightMsl = 0.0;

    osg::Vec3d normal;
    SGGeod geodPos = SGGeod::fromCart(toSG(cart));
    
    if (simgear::OsgEarthHeightCache::Instance()->FindTerrainHeightInScene(
        geodPos, heightMsl, normal)) 
    {
        v.z() = heightMsl - m_GeodPos.getElevationM();
    }
}


#endif